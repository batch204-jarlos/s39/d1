const jwt = require("jsonwebtoken");
// User defined string data that will be used to create our JSON web tokens
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = "CourseBookingAPI";


//JSON Web Tokens

//Token Creation
module.exports.createAccessToken = (user) => {
	console.log(user)
	/*
		{
		  _id: new ObjectId("634015b461e9860026ee3c44"),
		  firstName: 'Jane',
		  lastName: 'Hufano',
		  email: 'janehufano@mail.com',
		  password: '$2b$10$jgneYUAwtGwpbfY4VwqNg.5lFOekR.v1AS3kJlxNhX54.eVlp/Bd2',
		  isAdmin: false,
		  mobileNo: '09123456789',
		  enrollments: [],
		  __v: 0
		}
	*/
	// The data will be received from the registration form
	// When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//Generate a token
	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {})



}

//Token Verification
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization


	if (typeof token !== "undefined" ) {

		console.log(token)

		token = token.slice(7, token.length);


		return jwt.verify(token, secret, (err, data) => {

			if(err) {
				return res.send({auth: "failed"})
			} else {

				next()
			}

		})


	} else {

		return res.send({auth: "failed"});
	}

}




//Token Decryption
module.exports.decode = (token) => {

	if (typeof token !== "undefined") {
/*
Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzNDAxNWJjNWNhYWNlMGJkOWU0ZDgwNiIsImVtYWlsIjoiamFuZWh1ZmFub0BnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjY1NDA1Mjg0fQ.vn5UixGrLqn4EHMrCV8LZ3Oh-JhNbqN9Ffe7J2_nlTQ

*/

		token = token.slice(7, token.length);


		return jwt.verify(token, secret, (err, data) => {

				if(err) {
					return null

				} else {

					return jwt.decode(token, {complete: true}).payload;
				}

		})


	} else {

		return null
	}

}